//
//  NSString+Helper.h
//  Super
//
//  Created by Nikhil Bhownani on 16/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Helper)
+(NSString *)documentDirectoryPath;
+(NSString *)currentDateString ;
@end
