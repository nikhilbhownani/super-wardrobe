//
//  UIImage+Helper.h
//  Super
//
//  Created by Nikhil Bhownani on 16/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helper)
+ (UIImage *)fetchImageFromDocumentsDirectoryWithName:(NSString *)imageLocation;
@end
