//
//  Clothes.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Clothes.h"


@implementation Clothes

@dynamic imageLocation;
@dynamic name;
@dynamic clothDescription;

@end
