//
//  Clothes+CoreDataAddition.h
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Clothes.h"

@interface Clothes (CoreDataAddition)
+ (instancetype)itemWithName:(NSString *) name andLocation:(NSString *)imageLocation andDescription:(NSString *)description;
+ (NSArray *)fetchAllClothes ;
@end
