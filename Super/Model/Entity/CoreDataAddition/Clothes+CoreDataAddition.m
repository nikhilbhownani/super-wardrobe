//
//  Clothes+CoreDataAddition.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Clothes+CoreDataAddition.h"
#import "SuperModel.h"

@implementation Clothes (CoreDataAddition)
+ (instancetype)itemWithName:(NSString *) name andLocation:(NSString *)imageLocation andDescription:(NSString *)description {
  Clothes *clothesObject = (Clothes *)[NSEntityDescription entityForName:NSStringFromClass([Clothes class]) inManagedObjectContext:[SuperModel sharedInstance].managedObjectContext];
  clothesObject.imageLocation = imageLocation;
  clothesObject.clothDescription = description;
  return clothesObject;
}

+ (NSArray *)fetchAllClothes {
  NSManagedObjectContext *context = [SuperModel sharedInstance].managedObjectContext;
  NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([Clothes class]) inManagedObjectContext:context];
  NSFetchRequest *request = [[NSFetchRequest alloc] init];
  request.entity = entityDescription;
  
  NSError *error;
  NSArray *array =[context executeFetchRequest:request error:&error];
  
  return array;
}

+ (Clothes *)fetchRandomClothesObject {
  NSArray *array = [self fetchAllClothes];
  int randomValue = 0 + arc4random() % (array.count - 0);
  
  Clothes *cloth = [array objectAtIndex:randomValue];
  return cloth;
}
@end
