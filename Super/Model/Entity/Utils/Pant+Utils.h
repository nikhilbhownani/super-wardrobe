//
//  Pant+Utils.h
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Pant.h"
#import <UIKit/UIKit.h>

@interface Pant (Utils)
+ (instancetype)pantWithName:(NSString *) name
                       location:(NSString *)imageLocation
              andDescription:(NSString *)description;
+ (NSArray *)fetchAllPants ;
+ (instancetype)fetchRandomPant;
- (void)update;
@end
