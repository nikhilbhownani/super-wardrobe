//
//  Shirt+Utils.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Shirt+Utils.h"
#import "SuperModel.h"

@implementation Shirt (Utils)
+ (instancetype)shirtWithName:(NSString *)name location:(NSString *)imageLocation andDescription:(NSString *)description {
  NSManagedObjectContext *context = [SuperModel sharedInstance].managedObjectContext;
  Shirt *shirtObject = (Shirt *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Shirt class]) inManagedObjectContext:context];
  shirtObject.shirtName = name;
  shirtObject.imageLocation = imageLocation;
  shirtObject.shirtDescription = description;
  NSError *error;
  [context save:&error];
  return shirtObject;
}

+ (NSArray *)fetchAllShirts {
  NSManagedObjectContext *context = [SuperModel sharedInstance].managedObjectContext;
  NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([Shirt class]) inManagedObjectContext:context];
  NSFetchRequest *request = [[NSFetchRequest alloc] init];
  request.entity = entityDescription;
  
  NSError *error;
  NSArray *array =[context executeFetchRequest:request error:&error];
  
  return array;
}

+ (instancetype)fetchRandomShirt {
  NSArray *array = [Shirt fetchAllShirts];
  if (array.count==0) {
    return nil;
  }
  int randomValue = 0 + arc4random() % (array.count - 0);
  
  Shirt *shirt = [array objectAtIndex:randomValue];
  return shirt;
}

- (void)update {
  NSManagedObjectContext *context = [SuperModel sharedInstance].managedObjectContext;
  NSError *error;
  [context save:&error];
}

@end
