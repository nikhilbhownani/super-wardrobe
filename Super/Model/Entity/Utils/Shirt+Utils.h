//
//  Shirt+Utils.h
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Shirt.h"
#import <UIKit/UIKit.h>

@interface Shirt (Utils)
+ (instancetype)shirtWithName:(NSString *) name location:(NSString *)imageLocation andDescription:(NSString *)description;
+ (NSArray *)fetchAllShirts ;
+ (instancetype)fetchRandomShirt;
- (void)update;
@end
