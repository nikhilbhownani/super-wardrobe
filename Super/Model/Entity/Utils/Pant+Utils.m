//
//  Pant+Utils.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Pant+Utils.h"
#import "SuperModel.h"
@implementation Pant (Utils)
+ (instancetype)pantWithName:(NSString *) name location:(NSString *)imageLocation andDescription:(NSString *)description {
  NSManagedObjectContext *context = [SuperModel sharedInstance].managedObjectContext;
  Pant *pantObject = (Pant *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Pant class]) inManagedObjectContext:context];
  pantObject.pantName = name;
  
  pantObject.imageLocation = imageLocation;
  pantObject.pantDescription = description;
  NSError *error;
  [context save:&error];
  return pantObject;
}

+ (NSArray *)fetchAllPants {
  NSManagedObjectContext *context = [SuperModel sharedInstance].managedObjectContext;
  NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([Pant class]) inManagedObjectContext:context];
  NSFetchRequest *request = [[NSFetchRequest alloc] init];
  request.entity = entityDescription;
  
  NSError *error;
  NSArray *array =[context executeFetchRequest:request error:&error];
  
  return array;
}

+ (instancetype)fetchRandomPant {
  NSArray *array = [Pant fetchAllPants];
  if (array.count==0) {
    return nil;
  }
  int randomValue = 0 + arc4random() % (array.count - 0);
  
  Pant *pant = [array objectAtIndex:randomValue];
  return pant;
}

- (void)update {
  NSManagedObjectContext *context = [SuperModel sharedInstance].managedObjectContext;
  NSError *error;
  [context save:&error];
}


@end
