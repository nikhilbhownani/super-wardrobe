//
//  Pant.m
//  Super
//
//  Created by Nikhil Bhownani on 16/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Pant.h"


@implementation Pant

@dynamic pantDescription;
@dynamic pantName;
@dynamic imageLocation;

@end
