//
//  Pant.h
//  Super
//
//  Created by Nikhil Bhownani on 16/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Pant : NSManagedObject

@property (nonatomic, retain) NSString * pantDescription;
@property (nonatomic, retain) NSString * pantName;
@property (nonatomic, retain) NSString * imageLocation;

@end
