//
//  Shirt.h
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Shirt : NSManagedObject

@property (nonatomic, retain) NSString * shirtDescription;
@property (nonatomic, retain) NSString * shirtName;
@property (nonatomic, retain) NSString * imageLocation;

@end
