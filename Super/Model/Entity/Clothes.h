//
//  Clothes.h
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Clothes : NSManagedObject

@property (nonatomic, retain) NSString * imageLocation;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * clothDescription;

@end
