//
//  Shirt.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "Shirt.h"


@implementation Shirt

@dynamic shirtDescription;
@dynamic shirtName;
@dynamic imageLocation;

@end
