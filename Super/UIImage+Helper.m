//
//  UIImage+Helper.m
//  Super
//
//  Created by Nikhil Bhownani on 16/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "UIImage+Helper.h"
#import "NSString+Helper.h"
@implementation UIImage (Helper)
+ (UIImage *)fetchImageFromDocumentsDirectoryWithName:(NSString *)imageLocation {
  NSString *documentsDirectory = [NSString documentDirectoryPath];
  NSString *path = [documentsDirectory stringByAppendingPathComponent:
                    [NSString stringWithString: imageLocation] ];
  UIImage *image = [UIImage imageWithContentsOfFile:path];
  return image;
}
@end
