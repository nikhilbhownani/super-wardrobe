//
//  NSString+Helper.m
//  Super
//
//  Created by Nikhil Bhownani on 16/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "NSString+Helper.h"

@implementation NSString (Helper)
+(NSString *)documentDirectoryPath{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
  return documentsDirectory;
}

+ (NSString *)currentDateString {
  NSDateFormatter *formatter;
  NSString        *dateString;
  formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"ddMMyyyyHHmmssSSSSSS"];
  
  dateString = [formatter stringFromDate:[NSDate date]];
  return [dateString stringByAppendingString:@".png"];
  //  return dateString;
}
@end
