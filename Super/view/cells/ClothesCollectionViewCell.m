//
//  ClothesCollectionViewCell.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "ClothesCollectionViewCell.h"
#import "Shirt+Utils.h"
#import "Pant+Utils.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"
@interface ClothesCollectionViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *clothesImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) id object;

@end
@implementation ClothesCollectionViewCell

-(void)setupView:(id)object {
  self.object = object;
  UIImage *image;
  NSString *path ;
  if ([object isKindOfClass:[Shirt class]]) {
    path = ((Shirt *)object).imageLocation;
    self.nameLabel.text = [((Shirt *)object).shirtName uppercaseString];
  } else if([object isKindOfClass:[Pant class]]){
    path = ((Pant *)object).imageLocation;
    self.nameLabel.text = [((Pant *)object).pantName uppercaseString];
  }
   image = [UIImage fetchImageFromDocumentsDirectoryWithName:path];
  [self.clothesImageView setImage:image];

}
- (IBAction)deleteButtonTapped:(id)sender {
  [self.delegate handleDeleteCloth:self];
}

-(id)getObject {
  return self.object;
}
@end
