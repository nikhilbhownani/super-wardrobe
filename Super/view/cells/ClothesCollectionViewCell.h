//
//  ClothesCollectionViewCell.h
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ClothesCollectionViewCell;
@protocol ClothesCollectionViewCellDelegate <NSObject>
-(void)handleDeleteCloth:(ClothesCollectionViewCell *)cell;
@end

@interface ClothesCollectionViewCell : UICollectionViewCell
-(void)setupView:(id)object;
-(id)getObject;
@property (nonatomic, weak) id<ClothesCollectionViewCellDelegate> delegate;
@end
