//
//  DailyChoiceViewController.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "DailyChoiceViewController.h"
#import "Shirt+Utils.h"
#import "Pant+Utils.h"
#import "NSString+Helper.h"
#import "UploadImageViewController.h"
#import "UIImage+Helper.h"
@interface DailyChoiceViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *shirtImageView;
@property (weak, nonatomic) IBOutlet UIImageView *lowerImageView;
@property (weak, nonatomic) IBOutlet UILabel *suggestionLabel;

@end

@implementation DailyChoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setupView {
  
  Shirt *shirt = [Shirt fetchRandomShirt];
  Pant *pant = [Pant fetchRandomPant];
  if (!shirt || !pant) {
    [self showBlankDatabaseMessage];
    return;
  }
  UIImage *image = [UIImage fetchImageFromDocumentsDirectoryWithName:shirt.imageLocation];
  [self.shirtImageView setImage:image];
  image = [UIImage fetchImageFromDocumentsDirectoryWithName:pant.imageLocation];
  [self.lowerImageView setImage:image];
  
  self.suggestionLabel.text = [NSString stringWithFormat:@"We suggest you wear  %@ and %@", shirt.shirtName, pant.pantName];
  [self.view layoutIfNeeded];
  
}
- (UIStatusBarStyle) preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}
- (IBAction)myCollectionButtonTapped:(id)sender {
  UINavigationController *navController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ClothesListNavController"];
  [self presentViewController:navController animated:YES completion:^{
    
  }];
}

- (IBAction)newSuggestionButtonTapped:(id)sender {
  [self setupView];
}


- (void)showBlankDatabaseMessage {
  self.suggestionLabel.text = @"Welcome to the Super Wardrone Manager App. Click on the button to add items to your wardrobe.";
}
- (IBAction)plusButtonTapped:(id)sender {
  UploadImageViewController *uploadVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([UploadImageViewController class])];
  [self presentViewController:uploadVC animated:YES
completion:^{
  
}];
}
@end
