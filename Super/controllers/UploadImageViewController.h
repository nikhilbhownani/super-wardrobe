//
//  UploadImageViewController.h
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UploadImageViewController;

@protocol UploadImageViewControllerDelegate <NSObject>

- (void)handleNewImageAdded:(UploadImageViewController *)uploadViewController;

@end

@interface UploadImageViewController : UIViewController
@property (nonatomic, weak) id<UploadImageViewControllerDelegate> delegate;
- (void)setupData:(id)object;
- (void)setupView:(id)object;
@end
