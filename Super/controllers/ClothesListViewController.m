//
//  ClothesListViewController.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "ClothesListViewController.h"
#import "ClothesCollectionViewCell.h"
#import "UploadImageViewController.h"
#import "Pant+Utils.h"
#import "Shirt+Utils.h"
#import "SuperModel.h"

@interface ClothesListViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UploadImageViewControllerDelegate, ClothesCollectionViewCellDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *data;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circularViewWidthContraint;
@property (assign, nonatomic) BOOL isBeingDisplayedShirt;
@property (weak, nonatomic) IBOutlet UIButton *switchClothesCategoryButton;

@end

@implementation ClothesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.isBeingDisplayedShirt = YES;
  [self setupNavigationBar];
  [self setupCollectionView];
  [self setupData];
  [self setupCircularViewButton];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setupData {
  
  if (self.isBeingDisplayedShirt) {
    self.data = [Shirt fetchAllShirts];
  } else {
    self.data = [Pant fetchAllPants];
  }
  
}

- (void)setupCircularViewButton {
  self.switchClothesCategoryButton.layer.cornerRadius = self.circularViewWidthContraint.constant/2;
  UIImage *image = self.isBeingDisplayedShirt?[UIImage imageNamed:@"shirt-con"]:[UIImage imageNamed:@"pant-icon"] ;
  [self.switchClothesCategoryButton setImage:image forState:UIControlStateNormal];
}

- (void)setupNavigationBar {
  self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:22/255.0 green:42/255.0 blue:84/255.0 alpha:1.0];
  self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
  self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
  self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
  UIButton *closeButton = [[UIButton alloc] init];
  closeButton.frame = CGRectMake(0, 0, 23, 23);
  [closeButton addTarget:self action:@selector(handleCloseButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
  [closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

- (void) setupCollectionView {
  self.collectionView.delegate = self;
  self.collectionView.dataSource = self;
}

- (void)handleCloseButtonTapped: (id)sender {
  [self dismissViewControllerAnimated:YES
                           completion:^{
  
                           }];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
  return self.data.count;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  UploadImageViewController *uploadVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([UploadImageViewController class])];
  [uploadVC setupData:[self.data objectAtIndex:indexPath.row]];
  [self presentViewController:uploadVC animated:YES completion:^{
    
  }];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  ClothesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ClothesCollectionViewCell class]) forIndexPath:indexPath];
  [cell setupView:[self.data objectAtIndex:indexPath.row]];
  cell.delegate = self;
  return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return CGSizeMake(self.view.frame.size.width/2 - 20, 200);
}
- (IBAction)switchClothesButtonTapped:(id)sender {
  self.isBeingDisplayedShirt = !self.isBeingDisplayedShirt;
  [self setupCircularViewButton];
  [self setupData];
  [self.collectionView reloadData];
}

- (IBAction)addNewItemTapped:(id)sender {
  UploadImageViewController *uploadImageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([UploadImageViewController class])];
  uploadImageViewController.delegate = self;
  [self presentViewController:uploadImageViewController animated:YES completion:^{
  }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (buttonIndex == 0) {
    
    [self shouldStartCameraController];
  } else if (buttonIndex == 1) {
    [self shouldStartPhotoLibraryPickerController];
  }
}


- (void)shouldStartCameraController {
  UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
  pickerController.delegate = self;
  pickerController.allowsEditing = YES;
  
  pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
  pickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
  [self presentViewController:pickerController animated:YES
                   completion:^ {
                     [pickerController takePicture];
                   }];

}

- (void)shouldStartPhotoLibraryPickerController {
  UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
  pickerController.delegate = self;
  pickerController.allowsEditing = YES;
  
  pickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
//  pickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
  [self presentViewController:pickerController animated:YES
                   completion:^ {
//                     [pickerController takePicture];
                   }];
  

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
  UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
  
  if (!selectedImage) {
    selectedImage = info[UIImagePickerControllerOriginalImage];
  }
  
//  self.selectedImage = selectedImage;
  
//  [[[ImageCacheHelper alloc] initWithImage:selectedImage] saveImages];
  
//  [self.imagePickerButton setImage:selectedImage forState:UIControlStateNormal];
  [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)handleNewImageAdded:(UploadImageViewController *)uploadViewController {
  [self setupData];
  [self.collectionView reloadData];
}


#pragma mark - ClothesCollectionCell Delegate methods

- (void)handleDeleteCloth:(ClothesCollectionViewCell *)cell {
  [UIView animateWithDuration:0.5 animations:^{
    cell.alpha = 0.0;
  } completion:^(BOOL finished) {
    id object = [cell getObject];
    [[SuperModel sharedInstance].managedObjectContext deleteObject:object];
    NSError *error;
    [[SuperModel sharedInstance].managedObjectContext save:&error];
    [self setupData];
    [self.collectionView reloadData];
  }];
}
@end
