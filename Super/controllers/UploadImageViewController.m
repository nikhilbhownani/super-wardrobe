//
//  UploadImageViewController.m
//  Super
//
//  Created by Nikhil Bhownani on 15/04/15.
//  Copyright (c) 2015 Nikhil Bhownani. All rights reserved.
//

#import "UploadImageViewController.h"
#import "Pant+Utils.h"
#import "Shirt+Utils.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"

@interface UploadImageViewController ()<UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (nonatomic, strong) UIImage *selectedImage;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIPickerView *clothTypePicker;
@property (nonatomic, strong) NSString *clothType;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (nonatomic, strong) NSArray *clothTypeArray;
@property (nonatomic, strong) id data;
@end

@implementation UploadImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  [self setupPicker];
  [self setupClothTypes];
  
  [self textFields];
  if (self.data) {
    [self setupView];
  }
  
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)textFields {
  self.nameTextField.delegate = self;
  self.descriptionTextField.delegate = self;
}

- (void)setupClothTypes {
  self.clothTypeArray = @[@"Shirt", @"Pant"];
}

- (void)setupData:(id)object{
  self.data = object;
}

- (void)setupView{
  UIImage *image;
  NSString *path;
  if ([self.data isKindOfClass:[Shirt class]]){
    Shirt *shirt = self.data;
    self.nameTextField.text = shirt.shirtName;
    self.descriptionTextField.text = shirt.shirtDescription;
    
    path = shirt.imageLocation;
    [self.clothTypePicker selectRow:0 inComponent:0 animated:YES];
    
  } else {
    Pant *pant = self.data;
    self.nameTextField.text = pant.pantName;
    self.descriptionTextField.text = pant.pantDescription;
    path = pant.imageLocation;
    [self.clothTypePicker selectRow:1 inComponent:0 animated:YES];
  }
  image = [UIImage fetchImageFromDocumentsDirectoryWithName:path];
  [self.selectedImageView setImage:image];
//  [self.selectedImageView clipsToBounds];

  [self.view layoutIfNeeded];
}

- (void)setupPicker {
  self.clothTypePicker.dataSource = self;
  self.clothTypePicker.delegate = self;
}
- (IBAction)cameraButtonTapped:(id)sender {
  UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
  [actionSheet showInView:self.view];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  return YES;
}


#pragma mark - PickerView Delegate, Datasource
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
  return 2;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
  return 1;
}
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
  NSString *title;
  title = [self.clothTypeArray objectAtIndex:row];
  NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
  
  
  return attString;
  
}


- (IBAction)saveButtonTapped:(id)sender {
  
  BOOL validate = [self validateUserInput];
  self.clothType = [self.clothTypeArray objectAtIndex: [self.clothTypePicker selectedRowInComponent:0]];
  if (validate) {
    if ([self.clothType isEqualToString:@"Shirt"]) {
      [self saveShirtObject];
    } else if ([self.clothType isEqualToString:@"Pant"]){
      [self savePantObject];
    }
    [self dismissViewControllerAnimated:YES
                             completion:^{
                               [self.delegate handleNewImageAdded:self];
                             }];
  } else {
    [[[UIAlertView alloc ] initWithTitle:@"Error" message:@"Please input a valid image, and a valid description" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
  }
}
- (IBAction)cancelButtonTapped:(id)sender {
  [self dismissViewControllerAnimated:YES completion:^{
//    [self.delegate handleNewImageAdded:self];
  }];
}

- (BOOL)validateUserInput {
  return (self.descriptionTextField.text.length>0) && self.selectedImage;
//  return NO;
}


- (void)savePantObject {
    NSString *randomNameString = [NSString currentDateString];
  [self saveImageToDocumentsDirectoryWithName:randomNameString];
  if (self.data) {
    Pant *pant = self.data;
    pant.imageLocation = randomNameString;
    pant.pantName = self.nameTextField.text;
    pant.pantDescription = self.descriptionTextField.text;
    [pant update];
  } else {
    [Pant pantWithName:self.nameTextField.text
            location:randomNameString
      andDescription:self.descriptionTextField.text];
  }
}

- (void)saveShirtObject {
  NSString *randomNameString = [NSString currentDateString];
  if (self.data) {
    Shirt *shirt = self.data;
    shirt.imageLocation = randomNameString;
    shirt.shirtName = self.nameTextField.text;
    shirt.shirtDescription = self.descriptionTextField.text;
    [shirt update];
  } else {
    [self saveImageToDocumentsDirectoryWithName:randomNameString];
    [Shirt shirtWithName:self.nameTextField.text location:randomNameString andDescription:self.descriptionTextField.text];
  }
}


- (void)saveImageToDocumentsDirectoryWithName:(NSString *)randomNameString{
  NSString *dataPath = [NSString documentDirectoryPath];
  dataPath = [dataPath stringByAppendingString:@"/"];
  NSString *imagePath = [dataPath stringByAppendingString:randomNameString];
  NSData *imageData = UIImagePNGRepresentation([self selectedImage]);
  [imageData writeToFile:imagePath atomically:YES];
}

#pragma mark - Camera Related Methods
- (void)shouldStartCameraController {
  BOOL isCameraAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
  if(isCameraAvailable == NO){
    [[[UIAlertView alloc ] initWithTitle:@"Error" message:@"A camera device is not available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    return;
  }
  UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
  pickerController.delegate = self;
  pickerController.allowsEditing = YES;
  pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
  pickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
  [self presentViewController:pickerController animated:YES
                   completion:^ {
                     [pickerController takePicture];
                   }];
  
}

- (void)shouldStartPhotoLibraryPickerController {
    BOOL isLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
  if(isLibraryAvailable == NO){
    [[[UIAlertView alloc ] initWithTitle:@"Error" message:@"Photo library of the  device is not accessible" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    return;
  }
  UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
  pickerController.delegate = self;
  pickerController.allowsEditing = YES;

  
  pickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
  //  pickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
  [self presentViewController:pickerController animated:YES
                   completion:^ {
                     //                     [pickerController takePicture];
                   }];
  
  
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
  UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
  
  if (!selectedImage) {
    selectedImage = info[UIImagePickerControllerOriginalImage];
  }
  
  self.selectedImage = selectedImage;
  [self.selectedImageView setImage:selectedImage];
  [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma  mark - ActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (buttonIndex == 0) {
    
    [self shouldStartCameraController];
  } else if (buttonIndex == 1) {
    [self shouldStartPhotoLibraryPickerController];
  }
}
- (UIStatusBarStyle) preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}





@end
